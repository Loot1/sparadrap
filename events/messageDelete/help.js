const { MessageEmbed } = require('discord.js')
const db = require("../../db.json")

module.exports = async (client,message) => {
	if(message.channel.type == 'dm' || message.type != 'DEFAULT' || !db["users"].find(d => d.channel == message.channel.id) || message.embeds.length !== 0) return
	const helplogsChannel = message.guild.channels.cache.get(db["settings"]["help"]["logs"])
	if(!helplogsChannel) return
	const embed = new MessageEmbed()
	.setColor('fffffa')
	.setTitle("Suppression d'un message")
	.setDescription(message.channel)
	.setTimestamp()
	const logs = await message.guild.fetchAuditLogs({limit:1,type:'MESSAGE_DELETE'}).then(audit => audit.entries.first())
	if(message.channel.id === logs.extra.channel.id && logs.createdTimestamp > (Date.now() - 3000)) {
		embed.setAuthor(logs.executor.tag,logs.executor.avatarURL())
		embed.addField('Auteur',message.author)
		embed.setFooter(`ID: ${logs.executor.id}`)
	} else {
		embed.setAuthor(message.author.tag,message.author.avatarURL())
		embed.setFooter(`ID: ${message.author.id}`)
	}
	if(message.content.length <= 1024) embed.addField('Contenu',message.content)
	helplogsChannel.send(embed)
}