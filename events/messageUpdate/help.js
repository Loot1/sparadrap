const { MessageEmbed } = require('discord.js')
const db = require("../../db.json")

module.exports = (client,oldMessage,newMessage) => {
	if(newMessage.channel.type == 'dm' || newMessage.author.bot || !db["users"].find(d => d.channel == oldMessage.channel.id) || !oldMessage.content || oldMessage.content === newMessage.content) return
    const helplogsChannel = oldMessage.guild.channels.cache.get(db["settings"]["help"]["logs"])
	if(!helplogsChannel) return
	const embed = new MessageEmbed()
	.setColor('fffffa')
	.setTitle("Modification d'un message")
	.setAuthor(oldMessage.author.tag,oldMessage.author.avatarURL())
	.setDescription(oldMessage.channel)
	.setTimestamp()
	.setFooter(`ID: ${oldMessage.author.id}`)
	if(oldMessage.content.length <= 1024 && newMessage.content.length <= 1024) embed.addFields({name:"Ancien contenu",value:oldMessage.content,inline:true},{name:"Nouveau contenu",value:newMessage.content,inline:true})
	else if(oldMessage.content.length <= 1024) embed.addField("Ancien contenu",oldMessage.content)
	else embed.addField("Nouveau contenu",newMessage.content)
	helplogsChannel.send(embed)
}