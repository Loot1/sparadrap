const { MessageEmbed } = require('discord.js')
const { writeFileSync } = require("fs")
const db = require("../../db.json")

module.exports = async (client,channel) => {
    if(channel.type == 'dm') return
    const data = db["users"].find(d => d.channel == channel.id)
    if(!data) return

    db["users"].splice(db["users"].indexOf(data),1)
    writeFileSync("./db.json",JSON.stringify(db,null,4))
	client.users.cache.get(data.user).send(new MessageEmbed()
	.setTitle("**Supression du salon**")
	.setDescription("La discussion est close, l'historique des messages a été supprimé définitivement. Si vous souhaitez réouvrir la discussion, il vous suffit de renvoyer un message, n'oubliez pas que vous serez comme une nouvelle personne.")
	.setTimestamp()
    )

    const helplogsChannel = channel.guild.channels.cache.get(db["settings"]["help"]["logs"])
    if(helplogsChannel) {
        const logs = await channel.guild.fetchAuditLogs({limit:1,type:'CHANNEL_DELETE'}).then(audit => audit.entries.first())
        const embed = new MessageEmbed()
        .setColor('RED')
        .setTitle("Suppression")
        .setDescription(channel)
        .setTimestamp()
        if(channel.id === logs.target.id && logs.createdTimestamp > (Date.now() - 3000)) {
            embed.setAuthor(logs.executor.tag,logs.executor.avatarURL())
            embed.setFooter(`ID: ${logs.executor.id}`)
        }
        helplogsChannel.send(embed)
    }
}