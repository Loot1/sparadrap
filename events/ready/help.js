const db = require("../../db.json")

module.exports = (client) => {
    db["users"].forEach((data) => client.channels.cache.get(data.channel).messages.fetch(data.message))

	setInterval(function () {
		const helplogsChannel = client.channels.cache.get(db["settings"]["help"]["logs"])
	    if (helplogsChannel) helplogsChannel.setTopic(`⚡ Actif(s) : ${client.channels.cache.get(db["settings"]["help"]["active"]).children.size} 🌬️ Inactif(s) : ${client.channels.cache.get(db["settings"]["help"]["inactive"]).children.size} 📤 Banni(s) : ${client.channels.cache.get(db["settings"]["help"]["banned"]).children.size}`)
	}, 900000)
}