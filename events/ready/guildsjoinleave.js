const db = require("../../db.json")

module.exports = (client) => {
	setInterval(function () {
		const guildsjoinleaveChannel = client.channels.cache.get(db["settings"]["guildsjoinleave"])
		if (guildsjoinleaveChannel) guildsjoinleaveChannel.setTopic(`🌐 Serveur(s) : ${client.guilds.cache.size} 👫 Utilisateur(s) : ${client.users.cache.size}`)
	}, 900000)
}