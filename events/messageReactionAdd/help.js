const { MessageEmbed } = require("discord.js")
const db = require("../../db.json")

function embedmodify(chnl,parentid,msgid,fields,reacts,color,title,user) {
    chnl.setParent(parentid)
    chnl.messages.fetch(msgid).then((msg) => msg.edit(new MessageEmbed(msg.embeds[0]).spliceFields(0,msg.embeds[0].fields.length,fields)
        ).then((mess) => {
            mess.reactions.removeAll()
            reacts.forEach((react) => mess.react(react))
        })
    )
    const helplogsChannel = chnl.guild.channels.cache.get(db["settings"]["help"]["logs"])
    if(helplogsChannel) {
        helplogsChannel.send(new MessageEmbed()
        .setColor(color)
        .setTitle(title)
        .setAuthor(user.tag,user.avatarURL())
        .setDescription(chnl)
        .setTimestamp()
        .setFooter(`ID: ${user.id}`)
        )
    }
}

function sendDM(client,userid,title,desc) {
    client.users.cache.get(userid).send(new MessageEmbed()
    .setTitle(title)
    .setDescription(desc)
    .setTimestamp()
    )
}

module.exports = (client,reaction,user) => {
    if(user.bot) return
    const chnl = reaction.message.channel
    const data = db["users"].find(d => d.channel == chnl.id)
    if(!data) return
    const emote = reaction.emoji.name
    if(chnl.parent.id == db["settings"]["help"]["active"]) {
        if(emote == "🌬️") embedmodify(chnl,db["settings"]["help"]["inactive"],data.message,[{name:"⚡",value:"Réactiver la discussion",inline:true},{name:"📤", value:"Bannir l'utilisateur",inline:true}],["⚡","📤"],'d40bec',"Inactivation par réaction",user)
        else if(emote == "📤") {
            embedmodify(chnl,db["settings"]["help"]["banned"],data.message,[{name:"📥",value:"Débannir l'utilisateur",inline:true}],["📥"],'159de0',"Bannissement",user)
            sendDM(client,data.user,"**Banissement**","Vous avez été banni du service.")
        } else reaction.message.reactions.resolve(reaction).remove()
    } else if(chnl.parent.id == db["settings"]["help"]["inactive"]) {
        if(emote == "⚡") embedmodify(chnl,db["settings"]["help"]["active"],data.message,[{name:"🌬️",value:"Discussion inactive",inline:true},{name:"📤", value:"Bannir l'utilisateur",inline:true}],["🌬️","📤"],'d40bec',"Réactivation par réaction",user)
        else if(emote == "📤") {
            embedmodify(chnl,db["settings"]["help"]["banned"],data.message,[{name:"📥",value:"Débannir l'utilisateur",inline:true}],["📥"],'159de0',"Bannissement",user)
            sendDM(client,data.user,"**Banissement**","Vous avez été banni du service.")
        } else reaction.message.reactions.resolve(reaction).remove()
    } else if(chnl.parent.id == db["settings"]["help"]["banned"]) {
        if(emote == "📥") {
            embedmodify(chnl,db["settings"]["help"]["active"],data.message,[{name:"🌬️",value:"Discussion inactive",inline:true},{name:"📤", value:"Bannir l'utilisateur",inline:true}],["🌬️","📤"],'159de0',"Débannissement",user)
            sendDM(client,data.user,"**Débanissement**","Vous êtes maintenant débanni du service.")
        } else reaction.message.reactions.resolve(reaction).remove()
    }
}