const { Collection,MessageEmbed } = require("discord.js")
const db = require("../../db.json")

function embedsender(message,color,title,desc,time) {
    message.channel.send(new MessageEmbed()
    .setColor(color) 
    .setTitle(title)
    .setDescription(desc)
    .setTimestamp()
    .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
	).then(m => {
		message.delete({timeout:time})
		m.delete({timeout:time})
	})
}

module.exports = async (client,message) => {
	if(message.author.bot || message.channel.type == "dm" || message.type != 'DEFAULT') return	
	const data = db["guilds"].find(d => d.guild === message.guild.id)
	const prefix = data ? data.prefix : process.env.DEFAULT_PREFIX
	if(!message.content.startsWith(prefix)) return
	const args = message.content.slice(prefix.length).trim().split(/ +/g)
	const name = args.shift().toLowerCase()
	const command = client.commands.get(name) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(name))
	if(!command) return
	if(command.permissions.client.length !== 0) {
		var list = command.permissions.client.filter((permission) => !message.guild.me.hasPermission(permission)).map((p) => `\`${p}\``)
		if(list.length !== 0) return embedsender(message,'553986','👾  Permissions nécessaires',list.length === 1 ? `Permission manquante au client : ${list}` : `Permissions manquantes au client : ${list}`,5000)
	}
	if(command.permissions.owneronly == true) {
		const app = await client.fetchApplication()
		if(message.author.id !== app.owner.id) return embedsender(message,'de2e43','🚫  Permissions insuffisantes','Permission manquante : `BOT_OWNER`',5000)
	}
	if(command.permissions.user.length !== 0) {
		var list = command.permissions.user.filter((permission) => !message.member.hasPermission(permission)).map((p) => `\`${p}\``)
		if(list.length !== 0) return embedsender(message,'de2e43','🚫  Permissions insuffisantes',list.length === 1 ? `Permission manquante : ${list}` : `Permissions manquantes : ${list}`,5000)
	}
	if(args.length < command.arguments) return embedsender(message,'ORANGE',((command.arguments - args.length) > 1) ? '🏷️  Arguments manquants' : '🏷️  Argument manquant',`Il manque ${((command.arguments - args.length) > 1) ? `${command.arguments - args.length} arguments` : '1 argument'}.\nUtilisation : \`${prefix}${name} ${command.usage}\``,5000)
	if(!client.cooldowns.has(command.name)) client.cooldowns.set(command.name,new Collection())
	const now = Date.now()
	const timestamps = client.cooldowns.get(command.name)
	const cooldown = command.cooldown
	if (cooldown !== 0) {
		const cooldownAmount = cooldown * 1000
		if (timestamps.has(message.author.id)) {
			const expirationTime = timestamps.get(message.author.id) + cooldownAmount
			if (now < expirationTime) {
				const timeLeft = expirationTime - now
				return embedsender(message,'a0a0a0','⏳  Temps de recharge',`Il vous faut encore attendre ${(timeLeft/1000).toFixed(1)} secondes avant d'utiliser la commande \`${command.name}\`.`,timeLeft+1000)
			}
		}
		timestamps.set(message.author.id,now)
		setTimeout(() => timestamps.delete(message.author.id),cooldownAmount)
	}
	try {
		command.execute(client,message,args)
	} catch (error) {
		message.channel.send(new MessageEmbed()
		.setColor('8d0a0a')
		.setTitle('💥  Erreur')
		.setDescription("Une erreur s'est produite, la commande ne s'est pas bien exécutée. Un rapport d'erreur a été transmis.")
		.setTimestamp()
		.setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
		)
		const app = await client.fetchApplication()
		const invite = await message.channel.createInvite({maxAge:0})
		app.owner.send(new MessageEmbed()
		.setColor('8d0a0a')
		.setTitle("💥  Rapport d'erreur")
		.addField('🌐  Serveur',`[${message.guild.name}](${invite} 'Clique pour rejoindre le serveur')`,true)
		.addField('👑  Propriétaire',message.guild.owner,true)
		.addField('👤  Utilisateur',message.author,true)
		.addField('📋  Erreur',`\`\`\`js\n${error.stack.split(')')[0]})\`\`\``)
		.setTimestamp()
		.setFooter(`ID: ${message.guild.id}`)
		)
	}
}