const { MessageEmbed } = require('discord.js')
const db = require("../../db.json")

module.exports = async (client,message) => {
    if(message.author.bot || message.channel.type == "dm" || message.type != 'DEFAULT') return
    if(message.mentions.users.size === 0 || message.mentions.users.first().id !== client.user.id || !message.content.startsWith(`<@!${client.user.id}>`)) return
    const data = db["guilds"].find(d => d.guild === message.guild.id)
    const invite = await client.channels.cache.get(db["settings"]["guildsjoinleave"]).createInvite({maxAge:0})
    const embed = new MessageEmbed()
    .setColor('BLUE')
    .setTitle('❔  Aide')
    .setDescription(`Préfixe : \`${data ? data.prefix : process.env.DEFAULT_PREFIX}\` | [Support](${invite} 'Clique pour rejoindre le serveur de support du bot')`)
    .setTimestamp()
    .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
    client.commands.filter((cmd) => (cmd.name != "help") && (cmd.permissions.owneronly === false)).forEach((cmd) => embed.addField(`\`${cmd.name}\``,cmd.description,true))
    message.channel.send(embed)
}