const { MessageEmbed } = require('discord.js')

module.exports = {
    name: 'invite',
    aliases: ['invite'],
    description: 'Pour inviter le bot sur votre serveur',
    cooldown: 5,
    permissions: {
        client: [],
        user: [],
        owneronly: false
    },
    arguments : 0,
    usage: '',
	async execute(client,message,args) {
        const invite = await client.generateInvite()
        const invitepermissions = await client.generateInvite({permissions:['CREATE_INSTANT_INVITE','VIEW_CHANNEL','SEND_MESSAGES','MANAGE_MESSAGES','EMBED_LINKS','READ_MESSAGE_HISTORY']})
        message.channel.send(new MessageEmbed()
        .setColor('fffffa')
        .setTitle('📩  Invitation')
        .setDescription("Toi aussi tu veux participer au projet, alors c'est très simple ! Il te suffit d'ajouter Sparadrap aux serveurs Discord que tu administres via le lien ci-dessous :D")
        .addField("Lien d'invitation",`[Clique ici](${invite} 'Clique, allez !')`,true)
        .addField("Lien d'invitation avec les permissions pré-configurées",`[Clique ici](${invitepermissions} 'Clique, allez !')`,true)
        .setTimestamp()
        .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
		)
	}
}
