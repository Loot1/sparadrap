const { MessageEmbed } = require('discord.js')
const { writeFileSync } = require("fs")
const db = require("../../../db.json")

function embedsender(message,color,title,desc,deletetime) {
    message.channel.send(new MessageEmbed()
    .setColor(color)
    .setTitle(title)
    .setDescription(desc)
    .setTimestamp()
    .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
	).then(m => {
        if(deletetime === true) {
            message.delete({timeout:5000})
            m.delete({timeout:5000})
        }
	})
}

module.exports = {
    name: 'prefix',
    aliases: ['préfix','prefixe','préfixe'],
    description: 'Gestion du préfixe du bot',
    cooldown: 2,
    permissions: {
        client: [],
        user: ['ADMINISTRATOR'],
        owneronly: false
    },
    arguments : 1,
    usage: '<préfixe>',
	execute(client,message,args) {
        const data = db["guilds"].find(d => d.guild === message.guild.id)
        const oldPrefix = data ? data.prefix : process.env.DEFAULT_PREFIX
        if(args[0].length > 10) return embedsender(message,'RED','🔣  Préfixe inchangé',`La longueur maximale du préfixe est de 10.`,true)
        const char = args[0].replace(RegExp(/(\w|[&~"éèçà#'{([|`^@)°\]=+}€,?;.:/!§*-])+/g),"")
        if(char.length !== 0) {
            const list = Array.from(new Set(char.split(""))).map((c) => `\`${c}\``)
            return embedsender(message,'RED','🔣  Préfixe inchangé',list.length === 1 ? `Le préfixe entré contient un caractère non accepté : ${list}` : `Le préfixe entré contient des caractères non acceptés : ${list}`,true)
        }
        if(oldPrefix == args[0]) return embedsender(message,'RED','🔣  Préfixe inchangé',`Le préfixe est déjà \`${oldPrefix}\`.`,true)
        if(data) {
            if(args[0] == process.env.DEFAULT_PREFIX) db["guilds"].splice(db["guilds"].indexOf(data),1)
            else data.prefix = args[0]
        } else db["guilds"].push({guild:message.guild.id,prefix:args[0]})
        writeFileSync("./db.json",JSON.stringify(db,null,4))
        embedsender(message,'GREEN','🔣  Préfixe changé',`Le préfixe est maintenant \`${args[0]}\`.`,false)
    }
}