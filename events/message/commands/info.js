const { MessageEmbed } = require('discord.js')

module.exports = {
    name: 'info',
    aliases: ['info'],
    description: 'Concept du bot',
    cooldown: 5,
    permissions: {
        client: [],
        user: [],
        owneronly: false
    },
    arguments : 0,
    usage: '',
	execute(client,message,args) {
        message.channel.send(new MessageEmbed()
        .setColor('fc80b3')
        .setTitle('💡  Concept')
        .setDescription("Sparadrap est un service d'aide et de soutien aux personnes qui ont besoin de conseils, de soutien, d'aide ou encore ont besoin de parler. Ce service est géré par deux équipes bien distinctes qui travaillent séparément, l'équipe générale qui assure le support et le développement du bot, la modération du serveur discord du service, la charte graphique, les partenariats et la communication du projet et l'équipe d'aide et de soutien qui assure le service.  Ce service est anonyme, gratuit, le fonctionnement du projet est totalement transparent, toute personne est en droit de poser des questions sur ce dernier. Bien que nous soyons ouverts au recrutement de professionnels de santé au sein de notre équipe d'aide et de soutien. Ce service ne se substitue en aucun cas à un suivi médical.\nL'anonymat est assuré par le bot, le principe est simple, l'utilisateur du service envoie un message au bot, le bot crée un canal privé sur le serveur discord du service, le nom du canal est généré aléatoirement, le canal n'est visible que par les membres de l'équipe d'aide et de soutien qui ne sont plus en test. Il n'y a aucun moyen d'identifier l'utilisateur du service. Dans le cadre du respect de vos données, nous vous laissons aussi la possibilité de demander la suppression du canal à votre interlocuteur, vous recevrez une notification une fois fait.")
        .setTimestamp()
        .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
		)
	}
}