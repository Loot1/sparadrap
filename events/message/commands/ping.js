module.exports = {
    name: 'ping',
    aliases: ['ping'],
    description: 'Test du temps de réponse du bot',
    cooldown: 5,
    permissions: {
        client: [],
        user: [],
        owneronly: false
    },
    arguments: 0,
    usage: '',
	execute(client,message,args) {
        message.channel.send('Ping...').then(m => m.edit(`Pong! (${m.createdTimestamp - message.createdTimestamp} ms)`))
	}
}
