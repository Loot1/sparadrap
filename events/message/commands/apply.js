const { MessageEmbed } = require('discord.js')

module.exports = {
    name: 'apply',
    aliases: ['apply'],
    description: 'Informations sur le recrutement des aidants',
    cooldown: 5,
    permissions: {
        client: [],
        user: [],
        owneronly: false
    },
    arguments : 0,
    usage: '',
	execute(client,message,args) {
        message.channel.send(new MessageEmbed()
        .setColor('fc80b3')
        .setTitle('👫  Recrutements')
        .setDescription("Les recrutements se déroulent sur un formulaire consultable par le directeur et les cadres du service d'aide, il n'y a aucun pré-requis mise à part de posséder l'application Discord. La première partie du formulaire consiste à apprendre à connaître le postulant, puis ensuite nous faisons un point sur votre état psychologique, cela se finit par une mise en situation.")
        .addField("Formulaire","[Clique ici](https://forms.gle/f78MCFp1YcbovbMY8 'Clique, allez !')")
        .setTimestamp()
        .setFooter(`ID: ${message.author.id}`,message.author.avatarURL())
		)
	}
}
