const { MessageEmbed } = require("discord.js")
const { writeFileSync } = require("fs")
const db = require("../../db.json")

function randomname(client) {
	var consonants = 'bcdfghjklmnpqrstvwxyz'.split('')
	length = parseInt(Math.random() * (9 - 5) + 5)
	rand = function(limit) {
		return Math.floor(Math.random()*limit)
	}
	vowels = 'aeiou'.split('')
	name = ''
	for (var i = 0;i<length/2;i++) {
		var randConsonant = consonants[rand(consonants.length)]
		name += (i===0) ? randConsonant.toUpperCase() : randConsonant
		name += i*2<length-1 ? vowels[rand(vowels.length)] : ''
	}
	;["active","banned","inactive"].forEach((category) => {
		if(client.channels.cache.get(db["settings"]["help"][category]).children.find(channel => channel.name == name)) return randomname(client)
	})
	return name
}

module.exports = async (client,message) => {
	if(message.author.bot) {
		if(message.author.id === client.user.id && message.channel.type != "dm" && message.type == 'PINS_ADD' && db["users"].find(d => d.channel == message.channel.id)) message.delete()
	} else if(message.type == 'DEFAULT') {
		if(message.channel.type === "dm") {
			const guild = client.channels.cache.get(db["settings"]["help"]["active"]).guild
			const data = db["users"].find(d => d.user == message.author.id)
			if(data) {
				const chnl = guild.channels.cache.get(data.channel)
				if(chnl.parent.id == db["settings"]["help"]["banned"]) return
				if(chnl.parent.id == db["settings"]["help"]["inactive"]) {
					chnl.setParent(db["settings"]["help"]["active"])
					chnl.messages.fetch(data.message).then((msg) => msg.edit(new MessageEmbed(msg.embeds[0]).spliceFields(0,msg.embeds[0].fields.length,[{name:"🌬️",value:"Discussion inactive",inline:true},{name:"📤", value:"Bannir l'utilisateur",inline:true}]))
					).then((mess) => {
						mess.reactions.removeAll()
						;["🌬️","📤"].forEach((react) => mess.react(react))
						const helplogsChannel = guild.channels.cache.get(db["settings"]["help"]["logs"])
						if(helplogsChannel) {
							helplogsChannel.send(new MessageEmbed()
							.setColor('d40bec')
							.setTitle("Réactivation par message externe")
							.setDescription(chnl)
							.setTimestamp()
							)
						}
					})
				}
				chnl.send(message.content && message.content || null,{files:message.attachments && message.attachments.map(a => a.url) || []})
			} else {
				guild.channels.create(randomname(client), {
					type: 'text',
					parent: db["settings"]["help"]["active"]
				}).then(chnl => {
					chnl.send(new MessageEmbed()
					.setTitle(`Panel de contrôle de la discussion avec ${name}`)
					.addFields({name:"🌬️",value:"Discussion inactive",inline:true},{name:"📤",value:"Bannir l'utilisateur",inline:true})
					.setTimestamp()
					).then((msg) => {
						db["users"].push({channel:chnl.id,message:msg.id,user:message.author.id})
						writeFileSync("./db.json",JSON.stringify(db,null,4))
						msg.pin()
						;["🌬️","📤"].forEach((react) => msg.react(react))
						msg.fetch()
					})
					chnl.send(message.content && message.content || null,{files:message.attachments && message.attachments.map(a => a.url) || []})
					const helplogsChannel = chnl.guild.channels.cache.get(db["settings"]["help"]["logs"])
					if(helplogsChannel) {
						helplogsChannel.send(new MessageEmbed()
						.setColor('GREEN')
						.setTitle("Création")
						.setDescription(chnl)
						.setTimestamp()
						)
					}
				})
			}
		} else {
			const data = db["users"].find(d => d.channel == message.channel.id)
			if(data) {
				if(message.channel.parent.id == db["settings"]["help"]["inactive"]) {
					message.channel.setParent(db["settings"]["help"]["active"])
					const msg = await message.channel.messages.fetch(data.message)
					msg.edit(new MessageEmbed(msg.embeds[0]).spliceFields(0,msg.embeds[0].fields.length,[{name:"🌬️",value:"Discussion inactive",inline:true},{name:"📤", value:"Bannir l'utilisateur",inline:true}])
					).then((mess) => {
						mess.reactions.removeAll()
						;["🌬️","📤"].forEach((react) => mess.react(react))
						const helplogsChannel = message.channel.guild.channels.cache.get(db["settings"]["help"]["logs"])
						if(helplogsChannel) {
							helplogsChannel.send(new MessageEmbed()
							.setColor('d40bec')
							.setTitle("Réactivation par message interne")
							.setAuthor(message.author.tag,message.author.avatarURL())
							.setDescription(message.channel)
							.setTimestamp()
							.setFooter(`ID: ${message.author.id}`)
							)
						}
					})
				} else if(message.channel.parent.id == db["settings"]["help"]["banned"]) return message.delete()
				if(!message.content.startsWith("#")) client.users.cache.get(data.user).send(message.content && message.content || null,{files:message.attachments && message.attachments.map(a => a.url) || []})
			}
		}
	}
}