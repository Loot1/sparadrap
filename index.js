require("dotenv").config({path:".env"})

const { Client,Collection } = require("discord.js")
const { readdirSync,statSync } = require("fs")

const client = new Client({disableMentions:'everyone'})
client.commands = new Collection()
client.cooldowns = new Collection()

function Handler(dir,list) {
    list = list || []
    readdirSync(dir).forEach(function(file) {
		if(statSync(dir+"/"+file).isDirectory()) list = Handler(dir+"/"+file,list)
		else if(file.endsWith(".js")) list.push(dir+"/"+file)
	})
	return list
}

readdirSync('./events').filter((dir) => !dir.endsWith(".js")).forEach((dir) => {
	readdirSync(`./events/${dir}/`).map((file) => {
		if(dir == "message" && file == "commands") Handler("./events/message/commands").forEach((cmd) => client.commands.set(cmd.replace("./events/message/commands/","").replace(".js",""),require(cmd)))
		else client.on(dir.split(".").shift(), (...args) => require(`./events/${dir}/${file}`)(client, ...args))
	})
})

client.login()